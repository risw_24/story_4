from django.db import models

class MataKuliah(models.Model):
    mata_kuliah = models.CharField(max_length = 30)
    dosen = models.CharField(max_length = 30)
    sks = models.CharField(max_length = 2)
    semester = models.CharField(max_length = 30)
    deskripsi = models.CharField(max_length = 100)
    ruangan = models.CharField(max_length = 20)
