from django.urls import path
from . import views

app_name = "story_5"

urlpatterns = [
    path('', views.mata_kuliah, name='mata_kuliah'),
    path('read', views.read, name='read'),
    path('read/<int:id>', views.read_one, name='read_one'),
    path('delete/<int:id>', views.delete, name='delete'),
]