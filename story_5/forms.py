from django import forms
from .models import MataKuliah

class Input(forms.ModelForm):
    class Meta:
        model = MataKuliah
        fields = ['mata_kuliah', 'dosen', 'sks', 'semester', 'deskripsi', 'ruangan']

        widgets = {
            'css' : forms.TextInput(attrs={'class': 'form-control',
                                        'type' : 'text',
                                        })
                    }


