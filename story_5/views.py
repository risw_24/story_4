from django.http import HttpResponse
from django.shortcuts import render, redirect
from .forms import Input
from .models import MataKuliah

def mata_kuliah(request):
    form = Input()
    if(request.method=="POST"):
        form = Input(request.POST)
        if(form.is_valid):
            form.save()
    
    return render(request,"mata_kuliah.html", {"form":form})

def read(request):
    jadwal = MataKuliah.objects.all()
    content = {"jadwal" : jadwal }
    return render(request,'jadwal.html',content)

def read_one(request, id):
    jadwal = MataKuliah.objects.get(pk = id)
    content = {"jadwal" : jadwal}
    return render(request, "satu_matkul.html", content)

def delete(request, id):
    MataKuliah.objects.filter(pk = id).delete()
    return redirect('/mata_kuliah/read')

