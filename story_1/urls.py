from django.urls import path
from . import views

app_name = "story_1"

urlpatterns = [
    path('', views.beta_version, name='beta'),
]