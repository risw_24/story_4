from django.http import HttpResponse
from django.shortcuts import render

def beta_version(request):
    return render(request,"beta_version.html")
