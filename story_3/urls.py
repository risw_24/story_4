from django.urls import path
from . import views

app_name = "story_3"

urlpatterns = [
    path('resume/',views.resume, name="resume"),
]