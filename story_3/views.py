from django.http import HttpResponse
from django.shortcuts import render

def resume(request):
    return render(request,"resume.html")
