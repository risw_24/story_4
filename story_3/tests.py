from django.test import TestCase,Client

class Testing123(TestCase):
    def test_apakah_url_ada(self):
        response = Client().get('/resume/resume/')
        self.assertEquals(response.status_code,200)

    def test_di_dalam_resume_ada_tulisan_Download(self):
        response = Client().get('/resume/resume/')
        isi_html = response.content.decode('utf8')
        self.assertIn("Download", isi_html)
        self.assertTemplateUsed(response, "resume.html")  
