from django.urls import include, path
from . import views

app_name = "story_6"

urlpatterns = [
    path('jadwalku/', views.jadwalku, name="jadwalku"),
    path('form-jadwalku/', views.form_jadwalku, name="form-jadwalku"),
    path('form-person/', views.form_person, name="form-person"),
    path('jadwalku/delete-jadwal/<id>',views.delete_jadwal, name="delete_jadwal"),
    path('jadwalku/delete-person/<id>',views.delete_person, name="delete_person"),
]