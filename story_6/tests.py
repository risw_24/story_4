from django.test import TestCase, Client, RequestFactory
from django.urls import resolve
from django.http import HttpRequest
from .views import jadwalku, delete_jadwal, delete_person
from .models import Jadwalku, Person
from .forms import Input_Jadwal, Input_Person

class Testing(TestCase):
    
    def test_url_jadwalku(self):
        response = Client().get('/jadwalku/')
        self.assertEqual(response.status_code, 200)

    def test_using_jadwalku_template(self):
        response = Client().get('/jadwalku/')
        self.assertTemplateUsed(response, 'jadwalku.html')

    def test_using_jadwalku_func(self):
        found = resolve('/jadwalku/')
        self.assertEqual(found.func, jadwalku)
    
    def test_model_jadwalku(self):
        #Creating a new activity
        new_jadwal = Jadwalku.objects.create(nama_jadwal="koding", deskripsi_jadwal="ampe malem")
            
        #Retrieving all available activity
        jumlah_jadwal= Jadwalku.objects.all().count()
        self.assertEqual(jumlah_jadwal,1)


    def test_form_validation_for_input_jadwal(self):
        form = Input_Jadwal(data={'nama_jadwal': 'ani', 'deskripsi_jadwal': 'wow'})
        self.assertTrue(form.is_valid())

    def test_form_validation_for_input_person(self):
        form = Input_Person(data={'nama': 'budi'})
        self.assertTrue(form.is_valid)

    def test_model_person(self):
        new_jadwal = Jadwalku.objects.create(nama_jadwal="koding", deskripsi_jadwal="ampe malem")
        new_person = Person.objects.create(nama_orang="budi",kegiatan=new_jadwal)
        jumlah_orang= Person.objects.all().count()
        self.assertEqual(jumlah_orang,1)

    def test_model_relation(self):
        Jadwalku.objects.create(nama_jadwal="koding", deskripsi_jadwal="ampe malem")
        jadwal = Jadwalku.objects.all()[0].id
        kegiatan = Jadwalku.objects.get(id = jadwal)
        person = Person.objects.create(nama_orang="budi",kegiatan=kegiatan)
        jumlah_orang= Person.objects.filter(kegiatan=kegiatan).count()
        self.assertEqual(jumlah_orang,1)

    def test_url_form_jadwalku(self):
        response = Client().get('/form-jadwalku/')
        self.assertEqual(response.status_code, 200)

    def test_using_form_jadwalku_template(self):
        response = Client().get('/form-jadwalku/')
        self.assertTemplateUsed(response, 'form-jadwalku.html')
    
    def test_view_form_jadwalku(self):
        nama = "bikin web"
        deskripsi = "buat tugas mandiri"
        response = Client().post('/form-jadwalku/',{"nama_jadwal": nama, "deskripsi_jadwal":deskripsi})
        self.assertEqual(response.status_code,200)
        html_response = response.content.decode('utf8')
        self.assertIn(nama,html_response)
        self.assertIn(deskripsi,html_response)

    def test_view_form_person(self):
        new_jadwal = Jadwalku.objects.create(nama_jadwal="koding", deskripsi_jadwal="ampe malem")
        nama = "budi"
        response = Client().post('/form-person/',{"nama_orang": "budi", "kegiatan":new_jadwal.id})
        self.assertEqual(response.status_code,200)
        html_response = response.content.decode('utf8')
        jumlah = Person.objects.all().count()
        self.assertEqual(jumlah,1)

    def test_story6_view_show_peserta(self):
        Jadwalku.objects.create(nama_jadwal='Test',deskripsi_jadwal = "testing")
        idpk = Jadwalku.objects.all()[0].id
        kegiatan = Jadwalku.objects.get(id=idpk)
        Person.objects.create(nama_orang='Budi', kegiatan=kegiatan)
        request = HttpRequest()
        response = jadwalku(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Budi', html_response)

    def test_delete_jadwal(self):
        self.factory = RequestFactory()
        jadwal = Jadwalku.objects.create(nama_jadwal='Test',deskripsi_jadwal = "testing")
        alamat = "/jadwalku/delete-kegiatan/" + str(jadwal.id)
        request = self.factory.get(alamat, {'id': jadwal.id})
        response = delete_jadwal(request, id=jadwal.id)
        self.assertEqual(response.status_code,302)

    def test_delete_person(self):
        self.factory = RequestFactory()
        jadwal = Jadwalku.objects.create(nama_jadwal='Test',deskripsi_jadwal = "testing")
        person = Person.objects.create(nama_orang='Budi', kegiatan=jadwal)
        alamat = "/jadwalku/delete-person/" + str(person.id)
        request = self.factory.get(alamat, {'id': person.id})
        response = delete_person(request, id= person.id)
        self.assertEqual(response.status_code,302)

     


    
# Create your tests here.
