from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render,redirect, get_object_or_404
from .models import Jadwalku,Person
from .forms import Input_Jadwal,Input_Person


def form_jadwalku(request):
    form = Input_Jadwal()
    if (request.method=='POST'):
        form = Input_Jadwal(request.POST)
        if(form.is_valid()):
            form.save()
    return render(request,"form-jadwalku.html",{"form":form})

def form_person(request):
    form = Input_Person()
    if (request.method=="POST"):
        form = Input_Person(request.POST)
        if(form.is_valid()):
            form.save()
    return render(request,"form-person.html",{"form":form})

def jadwalku(request):
    jadwal = Jadwalku.objects.all()
    data_jadwal = []
    for kegiatan in jadwal:
        persons = Person.objects.filter(kegiatan=kegiatan)
        list_peserta = []
        for person in persons:
            list_peserta.append(person)
        data_jadwal.append((kegiatan,list_peserta))
    return render(request,"jadwalku.html", {"daftar_kegiatan": data_jadwal})

def delete_jadwal(request, id):
    obj = get_object_or_404(Jadwalku,id=id)
    obj.delete()
    return HttpResponseRedirect('/jadwalku/')

def delete_person(request,id):
    obj = get_object_or_404(Person,id=id)
    obj.delete()
    return HttpResponseRedirect('/jadwalku/')
