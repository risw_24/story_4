from django.db import models

class Jadwalku(models.Model):
    nama_jadwal = models.CharField(max_length = 30)
    deskripsi_jadwal = models.TextField(max_length = 100)

    def __str__(self):
        return self.nama_jadwal
    # Create your models here.

class Person(models.Model):
    nama_orang = models.CharField(max_length = 30)
    kegiatan = models.ForeignKey(Jadwalku, on_delete=models.CASCADE)
