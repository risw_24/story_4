from django import forms
from .models import Jadwalku, Person

class Input_Jadwal(forms.ModelForm):
    class Meta:
        model = Jadwalku
        fields = ['nama_jadwal', 'deskripsi_jadwal']


class Input_Person(forms.ModelForm):
    class Meta:
        model = Person
        fields = ['nama_orang','kegiatan']